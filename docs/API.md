- [Talking to the API](#sec-1)

API docs coming soon. TODO add openapi-spec.

# Talking to the API<a id="sec-1"></a>

The FlatTrack API is available at `/api`. To talk to the FlatTrack API, it requires the header `Content-Type: application/json`
