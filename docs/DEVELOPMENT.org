#+TITLE: FlatTrack development
#+AUTHOR: Caleb Woodbine <calebwoodbine.public@gmail.com>

* Development cycle
In-cluster local development is recommended, use [[https://minikube.sigs.k8s.io][minikube]] or [[https://kind.sigs.k8s.io/][kind]].

** (almost) Automatic
Get tilt from [[https://tilt.dev][Tilt.dev]].
#+begin_src shell
tilt up --host 0.0.0.0
#+end_src

Build the backend:
#+begin_src shell
CGO_ENABLED=0 go build -a -installsuffix cgo -ldflags "-extldflags '-static' -s -w" -o flattrack ./main.go
#+end_src

Build the frontend:
#+name: Build the frontend
#+begin_src shell
npm run build
#+end_src

Notes:
- this is the quickest development cycle factoring:
  - multi-stage build
  - components
  - setup

** Manual
*** Bring up the deployments
   #+begin_src shell
   kubectl apply -k deployments/k8s-manifests/development/postgres
   #+end_src
   
*** Port-forward the database connection   
   #+begin_src shell
   kubectl -n flattrack-dev port-forward service/postgres 5432:5432
   #+end_src

*** Backend
    #+begin_src shell
    go build -o flattrack ./main.go && ./flattrack
    #+end_src

*** Frontend
Install frontend dependencies:
    #+name: Install frontend dependencies
    #+begin_src shell
    npm i
    #+end_src

Build the frontend:
    #+name: Build the frontend
    #+begin_src shell
    npm run build
    #+end_src


* Additional
*** Manually connecting to the Postgres database
   #+begin_src shell
   kubectl -n flattrack-dev exec -it postgres-0 -- psql
   #+end_src

*** Remove migrations   
   #+begin_src shell
   gomigrate -source "file://$(pwd)/migrations" -database postgres://flattrack:flattrack@localhost/flattrack?sslmode=disable down
   #+end_src

* Project structure and details
*** Code
*** API
Written in golang, the API is located in [[https://gitlab.com/flattrack/flattrack/-/tree/master/pkg/routes][pkg/routes]].

The features and areas are separated into packages.

**** Testing
Tests are located in [[https://gitlab.com/flattrack/flattrack/-/tree/master/test/backend/e2e][test/backend/e2e]]. So far there are only e2e tests for FlatTrack's API.

*** Frontend
Written in Vue.js + JavaScript, the frontend is located in [[https://gitlab.com/flattrack/flattrack/-/tree/master/test/frontend][web]].

The frontend makes requests to the backend to perform actions.

*** Database
The migrations and database structuring is located in [[https://gitlab.com/flattrack/flattrack/-/tree/master/migrations][migrations]].
Each table is created with [[https://github.com/golang-migrate/migrate][golang-migrate]].

*** Assets
Images are located in [[https://gitlab.com/flattrack/flattrack/-/tree/master/web/assets][web/assets]], these are used throughout the project (such as in the frontend, and readme).
* Docs
To run the docs in development, use:
#+begin_src sh
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs:ro,Z squidfunk/mkdocs-material
#+end_src
* Making a release checklist
Things to do before making a release:
- update helm chart version
- ensure docs represent the latest changes
- ensure linting passes

