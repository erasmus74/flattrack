- [Code](#sec-1)
  - [Testing](#sec-1-1)
- [Community](#sec-2)
  - [Engagement](#sec-2-1)
  - [Guidelines](#sec-2-2)
- [Artwork & Design](#sec-3)
- [Documentation](#sec-4)

How to contribute to FlatTrack.

# Code<a id="sec-1"></a>

For a dive into getting started in development, please read the [development docs](./DEVELOPMENT.md). A good place to start is the [GitLab issues, filtering by issues which are good for new contributors](https://gitlab.com/flattrack/flattrack/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Good%20for%20new%20contributors).

## Testing<a id="sec-1-1"></a>

Tests are written for the backend and frontend, please read the [testing docs](./TESTING.md).

# Community<a id="sec-2"></a>

## Engagement<a id="sec-2-1"></a>

Let others in the larger [FOSS](https://simple.wikipedia.org/wiki/Free_and_open-source_software) community know about FlatTrack.

Outside of the FOSS community:

-   finding the needs of those living in flats
-   get others to try it and find their thoughts on the project

## Guidelines<a id="sec-2-2"></a>

You may wish to contribute to the [community guidelines](./COMMUNITY.md).

# Artwork & Design<a id="sec-3"></a>

Although not essential, it is a good idea to have an idea for frontend web development technologies.

# Documentation<a id="sec-4"></a>

Wanna help improve the documentation of FlatTrack? For code related documentation the cycle is similar to the [development docs](./DEVELOPMENT.md).

For these docs:

-   go through the docs and see if all of the bases are covered (e.g: deployment, development, etc&#x2026;), then include what is not documented
-   consider new areas which could be documented
-   improve the quality of existing documentation
