- [TLDR;](#sec-1)

Responsibilities and expectations for contributing and interacting.

# TLDR;<a id="sec-1"></a>

Be excellent to one another.
