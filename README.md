<a href="http://www.gnu.org/licenses/agpl-3.0.html">
    <img src="https://img.shields.io/badge/License-AGPL--3.0-blue.svg" alt="License: AGPL-3.0" />
</a>
<a href="https://gitlab.com/flattrack/flattrack/releases">
    <img src="https://img.shields.io/badge/version-0.0.1--alpha11-1-brightgreen.svg" alt="Version 0.0.1-alpha11-1" />
</a>
<a href='https://ind.ie/ethical-design'>
    <img style='margin-left: auto; margin-right: auto;' alt='We practice Ethical Design' src='https://img.shields.io/badge/Ethical_Design-_▲_❤_-blue.svg'>
</a>
<br/>
<img alt="FlatTrackLogo" src="" width=100>

# FlatTrack

> Collaborate with your flatmates

## Features
- Shopping List
- Tasks (WIP - no progress)
- Noticeboard (WIP - no progress)
- Shared Calendar (WIP - no progress)
- Recipes (WIP - no progress)
- Flatmates (WIP - no progress)
- Highfives (WIP - no progress)

## Technologies
- [golang](https://golang.org) - backend
- [vuejs](https://vuejs.org) - frontend
- [gorilla/mux](https://github.com/gorilla/mux) - HTTP multiplexer
- [bulma](https://bulma.io/) + [buefy](https://buefy.org/) - CSS framework
- [vuematerial](http://vuematerial.io/) - CSS framework
- [axios](https://github.com/axios/axios) - client-side HTTP request library
- [ginkgo & gomega](https://onsi.github.io/ginkgo/) - tests

## Getting started
Various options are available for running a FlatTrack instance:
- [FlatTrack.io hosting](https://flattrack.io) (recommended; coming soon)
- [Self-hosted Kubernetes](docs/DEPLOYMENT.md#kubernetes-recommended) (recommended)
- [Self-hosted Docker-Compose](docs/DEPLOYMENT.md#docker-compose)
- [Self-hosted plain Ubuntu server](docs/DEPLOYMENT.md#plain-ubuntu-server)

## Documentation
To view the documentation, please check out the GitLab-hosted [FlatTrack docs](https://flattrack.gitlab.io/flattrack)

## Contribution
### Development
From code, to assets/artwork, to community, to documentation, there are many ways to contribute.  
To learn how to contribute, please refer to the [development+contribution documentation](docs/DEVELOPMENT.org).  
Looking for something to do? Check out the [FlatTrack issues](https://gitlab.com/flattrack/flattrack/-/issues) page.

## Community
Join FlatTrack's community to chip in and improve it!  
Please read [community docs](docs/COMMUNITY.org)

## License
Copyright 2019-2020 Caleb Woodbine.  
This project is licensed under the [AGPL-3.0](http://www.gnu.org/licenses/agpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).  
This program comes with absolutely no warranty.  
