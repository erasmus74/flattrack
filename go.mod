module gitlab.com/flattrack/flattrack

go 1.14

require (
	github.com/ddo/go-vue-handler v0.0.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-swagger/go-swagger v0.23.0 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.9.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/imdario/mergo v0.3.9
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.3.0
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/prometheus/client_golang v0.9.3
	github.com/rs/cors v1.7.0
	github.com/satori/go.uuid v1.2.0
)
